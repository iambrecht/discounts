<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase\ApiFunctionalTestCase;

class CalculateDiscountActionTest extends ApiFunctionalTestCase
{
    /**
     * @dataProvider scenarioDataProvider
     */
    public function test_if_discounts_are_returned($order, $expectedDiscounts)
    {
        $this->client->request('POST', "/discounts/calculate", [], [], [], $order);

        $response = $this->client->getResponse();

        $this->assertEquals($response->getStatusCode(), Response::HTTP_OK);
        $this->assertJson(
            $expectedDiscounts,
            $response->getContent()
        );
    }

    public function scenarioDataProvider()
    {
        return [
            $this->firstScenario(),
            $this->secondScenario(),
            $this->thirdScenario(),
            $this->fourthScenario()
        ];
    }

    private function firstScenario()
    {
        $order = <<<JSON
{
  "id": "1",
  "customer-id": "1",
  "items": [
    {
      "product-id": "B102",
      "quantity": "10",
      "unit-price": "4.99",
      "total": "49.90"
    }
  ],
  "total": "49.90"
}
JSON;

        $expectedDiscounts = <<<JSON
[
  {
    "message": "discount.product.switch.6_th_product_free",
    "discount": "4.99 EUR"
  }
]
JSON;

        return [
            $order,
            $expectedDiscounts
        ];
    }

    private function secondScenario()
    {
        $order = <<<JSON
{
  "id": "2",
  "customer-id": "2",
  "items": [
    {
      "product-id": "B102",
      "quantity": "5",
      "unit-price": "4.99",
      "total": "24.95"
    }
  ],
  "total": "24.95"
}
JSON;

        $expectedDiscounts = <<<JSON
[
  {
    "message": "discount.loyal_customer.10_percent",
    "discount": "2.50 EUR"
  }
]
JSON;

        return [
            $order,
            $expectedDiscounts
        ];
    }

    private function thirdScenario()
    {
        $order = <<<JSON
{
  "id": "3",
  "customer-id": "3",
  "items": [
    {
      "product-id": "A101",
      "quantity": "2",
      "unit-price": "9.75",
      "total": "19.50"
    },
    {
      "product-id": "A102",
      "quantity": "1",
      "unit-price": "49.50",
      "total": "49.50"
    }
  ],
  "total": "69.00"
}
JSON;

        $expectedDiscounts = <<<JSON
[
  {
    "message": "discount.product.tools.cheapest_free",
    "discount": "9.75 EUR"
  }
]
JSON;

        return [
            $order,
            $expectedDiscounts
        ];
    }

    private function fourthScenario()
    {
        $order = <<<JSON
{
  "id": "2",
  "customer-id": "2",
  "items": [
    {
      "product-id": "B102",
      "quantity": "10",
      "unit-price": "4.99",
      "total": "49.90"
    },
    {
      "product-id": "A101",
      "quantity": "2",
      "unit-price": "9.75",
      "total": "19.50"
    },
    {
      "product-id": "A102",
      "quantity": "1",
      "unit-price": "49.50",
      "total": "49.50"
    }
  ],
  "total": "118.90"
}
JSON;

        $expectedDiscounts = <<<JSON
[
  {
    "message": "discount.product.switch.6_th_product_free",
    "discount": "4.99 EUR"
  },
  {
    "message": "discount.loyal_customer.10_percent",
    "discount": "11.89 EUR"
  },
  {
    "message": "discount.product.tools.cheapest_free",
    "discount": "9.75 EUR"
  }
]
JSON;

        return [
            $order,
            $expectedDiscounts
        ];
    }
}
