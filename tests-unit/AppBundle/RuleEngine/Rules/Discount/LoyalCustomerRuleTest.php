<?php

namespace AppBundle\RuleEngine\Rules\Discount;

use AppBundle\Entity\Discount;
use AppBundle\Entity\Order;
use AppBundle\Entity\ProductCategory;
use AppBundle\Iterator\DiscountCollection;
use AppBundle\Iterator\OrderItemCollection;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Tests\Fixtures\CustomerFixtures;
use Tests\Fixtures\OrderFixtures;
use Tests\Fixtures\ProductFixtures;

class LoyalCustomerRuleTest extends TestCase
{
    /**
     * @dataProvider scenarioDataProvider
     */
    public function test_if_LoyalCustomerRule_behaves_as_expected(
        Order $order,
        DiscountCollection $expectedDiscounts
    ) {
        $this->assertEquals(
            $expectedDiscounts,
            (new LoyalCustomerRule())->evaluate($order)
        );
    }

    public function scenarioDataProvider()
    {
        return [
            $this->firstScenario(),
            $this->secondScenario()
        ];
    }

    public function firstScenario()
    {
        $customer = CustomerFixtures::getCustomer(1, 'test', new Money(100000, new Currency('EUR')));

        $orderItemsCollection = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct(1, ProductCategory::CATEGORY_SWITCHES, new Money(500, new Currency('EUR'))),
                10,
                new Money(500, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            $customer,
            $orderItemsCollection
        );

        $expectedDiscounts = (new DiscountCollection())
            ->addDiscount(new Discount(LoyalCustomerRule::DISCOUNT_MESSAGE, new Money(500, new Currency('EUR'))));

        return [
            $order,
            $expectedDiscounts
        ];
    }

    public function secondScenario()
    {
        $customer = CustomerFixtures::getCustomer(1, 'test', new Money(90000, new Currency('EUR')));

        $orderItemsCollection = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct(1, ProductCategory::CATEGORY_SWITCHES, new Money(500, new Currency('EUR'))),
                10,
                new Money(500, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            $customer,
            $orderItemsCollection
        );

        return [
            $order,
            new DiscountCollection()
        ];
    }
}
