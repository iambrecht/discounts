<?php

namespace AppBundle\RuleEngine\Rules\Discount;

use AppBundle\Entity\Discount;
use AppBundle\Entity\Order;
use AppBundle\Entity\ProductCategory;
use AppBundle\Iterator\DiscountCollection;
use AppBundle\Iterator\OrderItemCollection;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Tests\Fixtures\CustomerFixtures;
use Tests\Fixtures\OrderFixtures;
use Tests\Fixtures\ProductFixtures;

class SixthProductOfEachSwitchCategoryRuleTest extends TestCase
{
    /**
     * @dataProvider scenarioDataProvider
     */
    public function test_if_SixthProductOfEachSwitchCategoryRule_behaves_as_expected(
        Order $order,
        DiscountCollection $expectedDiscounts
    ) {
        $this->assertEquals(
            $expectedDiscounts,
            (new SixthProductOfEachSwitchCategoryRule())->evaluate($order)
        );
    }

    public function scenarioDataProvider()
    {
        return [
            $this->firstScenario(),
            $this->secondScenario(),
            $this->thirdScenario()
        ];
    }

    public function firstScenario()
    {
        $orderItemsCollection = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct(1, ProductCategory::CATEGORY_SWITCHES, new Money(500, new Currency('EUR'))),
                6,
                new Money(500, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            CustomerFixtures::getCustomer(1, 'test', new Money(5000, new Currency('EUR'))),
            $orderItemsCollection
        );

        $expectedDiscounts = (new DiscountCollection())
            ->addDiscount(new Discount(SixthProductOfEachSwitchCategoryRule::DISCOUNT_MESSAGE, new Money(500, new Currency('EUR'))));

        return [
            $order,
            $expectedDiscounts,
        ];
    }

    public function secondScenario()
    {
        $orderItemsCollection = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct(1, ProductCategory::CATEGORY_SWITCHES, new Money(500, new Currency('EUR'))),
                5,
                new Money(500, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            CustomerFixtures::getCustomer(1, 'test', new Money(5000, new Currency('EUR'))),
            $orderItemsCollection
        );

        return [
            $order,
            new DiscountCollection()
        ];
    }

    public function thirdScenario()
    {
        $orderItemsCollection = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct(1, ProductCategory::CATEGORY_SWITCHES, new Money(500, new Currency('EUR'))),
                6,
                new Money(500, new Currency('EUR'))
            ))
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct(2, ProductCategory::CATEGORY_SWITCHES, new Money(650, new Currency('EUR'))),
                6,
                new Money(650, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            CustomerFixtures::getCustomer(1, 'test', new Money(5000, new Currency('EUR'))),
            $orderItemsCollection
        );

        $expectedDiscounts = (new DiscountCollection())
            ->addDiscount(new Discount(SixthProductOfEachSwitchCategoryRule::DISCOUNT_MESSAGE, new Money(500, new Currency('EUR'))))
            ->addDiscount(new Discount(SixthProductOfEachSwitchCategoryRule::DISCOUNT_MESSAGE, new Money(650, new Currency('EUR'))));

        return [
            $order,
            $expectedDiscounts
        ];
    }
}
