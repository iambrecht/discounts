<?php

namespace AppBundle\RuleEngine\Rules\Discount;

use AppBundle\Entity\Discount;
use AppBundle\Entity\Order;
use AppBundle\Entity\ProductCategory;
use AppBundle\Iterator\DiscountCollection;
use AppBundle\Iterator\OrderItemCollection;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Tests\Fixtures\CustomerFixtures;
use Tests\Fixtures\OrderFixtures;
use Tests\Fixtures\ProductFixtures;

class TwoProductsOfToolsCategoryRuleTest extends TestCase
{
    /**
     * @dataProvider scenarioDataProvider
     */
    public function test_if_TwoProductsOfToolsCategoryRule_behaves_as_expected(
        Order $order,
        DiscountCollection $expectedDiscounts
    ) {
        $this->assertEquals(
            $expectedDiscounts,
            (new TwoProductsOfToolsCategoryRule())->evaluate($order)
        );
    }

    public function scenarioDataProvider()
    {
        return [
            $this->firstScenario(),
            $this->secondScenario(),
            $this->thirdScenario()
        ];
    }

    public function firstScenario()
    {
        $productOne = ProductFixtures::getProduct(1, ProductCategory::CATEGORY_TOOLS, new Money(500, new Currency('EUR')));
        $productTwo = ProductFixtures::getProduct(2, ProductCategory::CATEGORY_TOOLS, new Money(600, new Currency('EUR')));

        $orderItemsCollection = new OrderItemCollection();

        $orderItemsCollection
            ->addOrderItem(OrderFixtures::getOrderItem($productOne, 1, $productOne->getPrice()))
            ->addOrderItem(OrderFixtures::getOrderItem($productTwo, 1, $productTwo->getPrice()));

        $order = OrderFixtures::getOrder(1, CustomerFixtures::getCustomer(1, 'test', new Money(1000, new Currency('EUR'))), $orderItemsCollection);

        $expectedDiscounts = (new DiscountCollection())
            ->addDiscount(new Discount(TwoProductsOfToolsCategoryRule::DISCOUNT_MESSAGE, new Money(500, new Currency('EUR'))));

        return [
            $order,
            $expectedDiscounts
        ];
    }

    public function secondScenario()
    {
        $product = ProductFixtures::getProduct(1, ProductCategory::CATEGORY_TOOLS, new Money(500, new Currency('EUR')));

        $orderItemsCollection = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem($product, 2, $product->getPrice()));

        $order = OrderFixtures::getOrder(1, CustomerFixtures::getCustomer(1, 'test', new Money(1000, new Currency('EUR'))), $orderItemsCollection);

        $expectedDiscounts = (new DiscountCollection())
            ->addDiscount(new Discount(TwoProductsOfToolsCategoryRule::DISCOUNT_MESSAGE, new Money(500, new Currency('EUR'))));

        return [
            $order,
            $expectedDiscounts
        ];
    }

    public function thirdScenario()
    {
        $product = ProductFixtures::getProduct(1, ProductCategory::CATEGORY_TOOLS, new Money(500, new Currency('EUR')));

        $orderItemsCollection = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem($product, 1, $product->getPrice()));

        $order = OrderFixtures::getOrder(1, CustomerFixtures::getCustomer(1, 'test', new Money(1000, new Currency('EUR'))), $orderItemsCollection);

        return [
            $order,
            new DiscountCollection()
        ];
    }
}
