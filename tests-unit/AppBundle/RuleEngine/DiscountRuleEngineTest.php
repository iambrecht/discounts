<?php

namespace AppBundle\RuleEngine;

use AppBundle\Entity\Discount;
use AppBundle\Entity\Order;
use AppBundle\Entity\ProductCategory;
use AppBundle\Iterator\DiscountCollection;
use AppBundle\Iterator\OrderItemCollection;
use AppBundle\RuleEngine\Rules\Discount\LoyalCustomerRule;
use AppBundle\RuleEngine\Rules\Discount\SixthProductOfEachSwitchCategoryRule;
use AppBundle\RuleEngine\Rules\Discount\TwoProductsOfToolsCategoryRule;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Tests\Fixtures\CustomerFixtures;
use Tests\Fixtures\OrderFixtures;
use Tests\Fixtures\ProductFixtures;

class DiscountRuleEngineTest extends TestCase
{
    /**
     * @dataProvider scenarioDataProvider
     */
    public function test_if_discount_rule_engine_works(Order $order, DiscountCollection $expectedDiscounts)
    {
        $engine = (new DiscountRuleEngine())
            ->addRule(new SixthProductOfEachSwitchCategoryRule())
            ->addRule(new TwoProductsOfToolsCategoryRule())
            ->addRule(new LoyalCustomerRule());

        $this->assertEquals(
            $expectedDiscounts,
            $engine->generateDiscountsFromOrder($order)
        );
    }

    public function scenarioDataProvider()
    {
        return [
            $this->firstScenario(),
            $this->secondScenario(),
            $this->thirdScenario(),
            $this->fourthScenario(),
            $this->fifthScenario()
        ];
    }

    private function firstScenario()
    {
        $orderItems = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct('B102', ProductCategory::CATEGORY_SWITCHES, new Money(499, new Currency('EUR'))),
                10,
                new Money(499, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            CustomerFixtures::getCustomer(1, 'Coca Cola', new Money(49212, new Currency('EUR')), new \DateTime('2014-06-28')),
            $orderItems
        );

        $expectedDiscounts = (new DiscountCollection())
            ->addDiscount(new Discount(SixthProductOfEachSwitchCategoryRule::DISCOUNT_MESSAGE, new Money(499, new Currency('EUR'))));

        return [
            $order,
            $expectedDiscounts
        ];
    }

    private function secondScenario()
    {
        $orderItems = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct('B102', ProductCategory::CATEGORY_SWITCHES, new Money(499, new Currency('EUR'))),
                5,
                new Money(499, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            CustomerFixtures::getCustomer(2, 'Teamleader', new Money(150595, new Currency('EUR')), new \DateTime('2015-01-15')),
            $orderItems
        );

        $expectedDiscounts = (new DiscountCollection())
            ->addDiscount(new Discount(LoyalCustomerRule::DISCOUNT_MESSAGE, $order->getTotal()->multiply(0.1)));

        return [
            $order,
            $expectedDiscounts
        ];
    }

    private function thirdScenario()
    {
        $orderItems = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct('A101', ProductCategory::CATEGORY_TOOLS, new Money(975, new Currency('EUR'))),
                2,
                new Money(975, new Currency('EUR'))
            ))
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct('A102', ProductCategory::CATEGORY_TOOLS, new Money(4950, new Currency('EUR'))),
                1,
                new Money(4950, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            CustomerFixtures::getCustomer(3, 'Jeroen De Wit', new Money(0, new Currency('EUR')), new \DateTime('2016-02-11')),
            $orderItems
        );

        $expectedDiscounts = (new DiscountCollection())
            ->addDiscount(new Discount(TwoProductsOfToolsCategoryRule::DISCOUNT_MESSAGE, new Money(975, new Currency('EUR'))));

        return [
            $order,
            $expectedDiscounts
        ];
    }

    private function fourthScenario()
    {
        $orderItems = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct('A101', ProductCategory::CATEGORY_TOOLS, new Money(975, new Currency('EUR'))),
                2,
                new Money(975, new Currency('EUR'))
            ))
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct('A102', ProductCategory::CATEGORY_TOOLS, new Money(4950, new Currency('EUR'))),
                1,
                new Money(4950, new Currency('EUR'))
            ))
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct('B102', ProductCategory::CATEGORY_SWITCHES, new Money(499, new Currency('EUR'))),
                10,
                new Money(499, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            CustomerFixtures::getCustomer(2, 'Teamleader', new Money(150595, new Currency('EUR')), new \DateTime('2015-01-15')),
            $orderItems
        );

        $expectedDiscounts = (new DiscountCollection())
            ->addDiscount(new Discount(SixthProductOfEachSwitchCategoryRule::DISCOUNT_MESSAGE, new Money(499, new Currency('EUR'))))
            ->addDiscount(new Discount(TwoProductsOfToolsCategoryRule::DISCOUNT_MESSAGE, new Money(975, new Currency('EUR'))))
            ->addDiscount(new Discount(LoyalCustomerRule::DISCOUNT_MESSAGE, $order->getTotal()->multiply(0.1)));

        return [
            $order,
            $expectedDiscounts
        ];
    }

    private function fifthScenario()
    {
        $orderItems = (new OrderItemCollection())
            ->addOrderItem(OrderFixtures::getOrderItem(
                ProductFixtures::getProduct('B102', ProductCategory::CATEGORY_SWITCHES, new Money(499, new Currency('EUR'))),
                4,
                new Money(499, new Currency('EUR'))
            ));

        $order = OrderFixtures::getOrder(
            1,
            CustomerFixtures::getCustomer(1, 'Coca Cola', new Money(49212, new Currency('EUR')), new \DateTime('2014-06-28')),
            $orderItems
        );

        return [
            $order,
            new DiscountCollection()
        ];
    }
}
