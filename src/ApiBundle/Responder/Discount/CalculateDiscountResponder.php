<?php

namespace ApiBundle\Responder\Discount;

use ApiBundle\Responder\AbstractResponder;
use AppBundle\Entity\Discount;
use Symfony\Component\HttpFoundation\JsonResponse;

class CalculateDiscountResponder extends AbstractResponder
{
    public function __invoke()
    {
        $responseData = [];

        /** @var Discount $discount */
        foreach ($this->data as $discount) {
            $responseData[] = [
                'message' => $discount->getMessage(),
                'discount' => number_format ($discount->getDiscount()->getAmount() / 100, 2, ".", " ") .' ' . $discount->getDiscount()->getCurrency()
            ];
        }

        return new JsonResponse($responseData);
    }
}
