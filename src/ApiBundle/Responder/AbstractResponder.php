<?php

namespace ApiBundle\Responder;

abstract class AbstractResponder implements ResponderInterface
{
    protected $data;

    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    public abstract function __invoke();
}
