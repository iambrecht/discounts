<?php

namespace ApiBundle\Responder;

interface ResponderInterface
{
    public function setData(array $data);
    public function __invoke();
}
