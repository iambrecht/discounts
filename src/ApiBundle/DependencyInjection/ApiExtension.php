<?php

namespace ApiBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class ApiExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $this->loadServices($container);
        $this->checkAwareness($container);
    }

    private function loadServices(ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );

        $loader->load('services.yml');
    }

    protected function checkAwareness(ContainerBuilder $container)
    {
        $awarenessDefinitions = $this->getAwarenessDefinitions();

        foreach ($container->getDefinitions() as $serviceId => $definition) {
            $className = $container->getParameterBag()->resolveValue($definition->getClass());
            $classInterfaces = class_implements($className);

            foreach ($awarenessDefinitions as $awarenessDef) {
                if (in_array($awarenessDef['interface'], $classInterfaces)) {
                    $definition->addMethodCall($awarenessDef['setter'], array(new Reference($awarenessDef['service'])));
                }
            }
        }
    }

    protected function getAwarenessDefinitions()
    {
        return [
            [
                'interface' => 'ApiBundle\\DependencyInjection\\RequestAwareInterface',
                'service' => 'request_stack',
                'setter' => 'setRequest'
            ]
        ];
    }
}
