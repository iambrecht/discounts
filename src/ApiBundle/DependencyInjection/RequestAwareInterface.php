<?php

namespace ApiBundle\DependencyInjection;

use Symfony\Component\HttpFoundation\RequestStack;

interface RequestAwareInterface
{
    public function setRequest(RequestStack $requestStack);
}
