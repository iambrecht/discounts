<?php

namespace ApiBundle\Action\Discount;

use ApiBundle\Action\AbstractAction;
use ApiBundle\Responder\ResponderInterface;
use AppBundle\Mapper\OrderMapper;
use AppBundle\RuleEngine\DiscountRuleEngine;

class CalculateDiscountAction extends AbstractAction
{
    private
        $orderMapper,
        $discountEngine,
        $responder;

    public function __construct(
        OrderMapper $orderMapper,
        DiscountRuleEngine $discountEngine,
        ResponderInterface $responder
    ) {
        $this->orderMapper = $orderMapper;
        $this->discountEngine = $discountEngine;
        $this->responder = $responder;
    }

    public function __invoke()
    {
        $order = $this->orderMapper->transformObject(json_decode($this->request->getContent(), true));

        $discounts = $this->discountEngine->generateDiscountsFromOrder($order);

        $this->responder->setData($discounts->toArray());

        return $this->responder->__invoke();
    }
}
