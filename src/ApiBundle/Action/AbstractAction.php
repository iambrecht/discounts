<?php

namespace ApiBundle\Action;

use ApiBundle\DependencyInjection\RequestAwareInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class AbstractAction implements RequestAwareInterface
{
    /** @var Request $request */
    protected $request;

    public function setRequest(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }
}
