<?php

namespace AppBundle\RuleEngine;

use AppBundle\Entity\Order;
use AppBundle\Iterator\DiscountCollection;
use AppBundle\RuleEngine\Rules\RuleInterface;

class DiscountRuleEngine
{
    private $rules;

    public function __construct()
    {
        $this->rules = [];
    }

    public function addRule(RuleInterface $rule)
    {
        $this->rules[] = $rule;

        return $this;
    }

    public function generateDiscountsFromOrder(Order $order)
    {
        $discounts = new DiscountCollection();
        foreach ($this->rules as $rule) {
            $evaluatedDiscounts = $rule->evaluate($order);
            if ($evaluatedDiscounts->count() > 0) {
                $discounts->merge($evaluatedDiscounts);
            }
        }

        return $discounts;
    }
}
