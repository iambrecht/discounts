<?php

namespace AppBundle\RuleEngine\Rules;

use AppBundle\Entity\Order;

interface RuleInterface
{
    public function evaluate(Order $order);
}
