<?php

namespace AppBundle\RuleEngine\Rules\Discount;

use AppBundle\Entity\Order;
use AppBundle\Entity\ProductCategory;
use AppBundle\Iterator\DiscountCollection;

class SixthProductOfEachSwitchCategoryRule extends AbstractDiscountRule
{
    const DISCOUNT_MESSAGE = 'discount.product.switch.6_th_product_free';

    public function evaluate(Order $order)
    {
        $discountCollection = new DiscountCollection();
        foreach ($order->getOrderItems()->withCategory(ProductCategory::CATEGORY_SWITCHES) as $product) {
            if ($product->getQuantity() >= 6) {
                $discountCollection->addDiscount(
                    $this->getDiscount(self::DISCOUNT_MESSAGE, $product->getUnitPrice())
                );
            }
        }

        return $discountCollection;
    }
}
