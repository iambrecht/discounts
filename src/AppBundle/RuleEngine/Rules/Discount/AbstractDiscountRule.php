<?php

namespace AppBundle\RuleEngine\Rules\Discount;

use AppBundle\Entity\Discount;
use AppBundle\RuleEngine\Rules\RuleInterface;

abstract class AbstractDiscountRule implements RuleInterface
{
    protected function getDiscount($message, $discountAmount)
    {
        return new Discount($message, $discountAmount);
    }
}
