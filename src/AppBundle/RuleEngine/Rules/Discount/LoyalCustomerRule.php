<?php

namespace AppBundle\RuleEngine\Rules\Discount;

use AppBundle\Entity\Order;
use AppBundle\Iterator\DiscountCollection;
use Money\Currency;
use Money\Money;

class LoyalCustomerRule extends AbstractDiscountRule
{
    const DISCOUNT_MESSAGE = 'discount.loyal_customer.10_percent';

    public function evaluate(Order $order)
    {
        $discountCollection = new DiscountCollection();
        if ($order->getCustomer()->getRevenue()->greaterThanOrEqual(new Money(100000, new Currency('EUR')))) {
            $discountCollection->addDiscount(
                $this->getDiscount(self::DISCOUNT_MESSAGE, $order->getTotal()->multiply(0.1))
            );
        }

        return $discountCollection;
    }
}
