<?php

namespace AppBundle\RuleEngine\Rules\Discount;

use AppBundle\Entity\Order;
use AppBundle\Entity\ProductCategory;
use AppBundle\Iterator\DiscountCollection;

class TwoProductsOfToolsCategoryRule extends AbstractDiscountRule
{
    const DISCOUNT_MESSAGE = 'discount.product.tools.cheapest_free';

    public function evaluate(Order $order)
    {
        $products = $order->getOrderItems()->withCategory(ProductCategory::CATEGORY_TOOLS);

        $discountCollection = new DiscountCollection();
        if (
            $products->count() >= 2
            || (
                $products->count() === 1
                && $products->toArray()[0]->getQuantity () >= 2
            )
        ) {
            $cheapestProduct = $products->toArray()[0];
            foreach ($products as $product) {
                if ($product->getUnitPrice()->lessThanOrEqual($cheapestProduct->getUnitPrice())) {
                    $cheapestProduct = $product;
                }
            }

            $discountCollection->addDiscount(
                $this->getDiscount(self::DISCOUNT_MESSAGE, $cheapestProduct->getUnitPrice())
            );
        }

        return $discountCollection;
    }
}
