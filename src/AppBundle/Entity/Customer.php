<?php

namespace AppBundle\Entity;

use Money\Money;

class Customer
{
    private
        $id,
        $name,
        $since,
        $revenue;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSince()
    {
        return $this->since;
    }

    /**
     * @param \DateTime $since
     *
     * @return $this
     */
    public function setSince(\DateTime $since)
    {
        $this->since = $since;

        return $this;
    }

    /**
     * @return Money
     */
    public function getRevenue()
    {
        return $this->revenue;
    }

    /**
     * @param Money $revenue
     *
     * @return $this
     */
    public function setRevenue(Money $revenue)
    {
        $this->revenue = $revenue;

        return $this;
    }
}
