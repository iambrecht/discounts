<?php

namespace AppBundle\Entity;

use AppBundle\Iterator\OrderItemCollection;
use Money\Money;

class Order
{
    private
        $id,
        $customer,
        $orderItems,
        $total;

    public function __construct()
    {
        $this->orderItems = new OrderItemCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     *
     * @return $this
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return OrderItemCollection
     */
    public function getOrderItems()
    {
        return $this->orderItems;
    }

    /**
     * @param OrderItemCollection $orderItems
     *
     * @return $this
     */
    public function setOrderItems(OrderItemCollection $orderItems)
    {
        $this->orderItems = $orderItems;

        return $this;
    }

    /**
     * @param OrderItem $product
     *
     * @return $this
     */
    public function addOrderItem(OrderItem $product)
    {
        $this->orderItems->addOrderItem($product);

        return $this;
    }

    /**
     * @return Money
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param Money $total
     *
     * @return $this
     */
    public function setTotal(Money $total)
    {
        $this->total = $total;

        return $this;
    }
}
