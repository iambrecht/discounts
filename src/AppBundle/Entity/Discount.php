<?php

namespace AppBundle\Entity;

use Money\Money;

class Discount
{
    private
        $message,
        $discount;

    public function __construct(
        $message,
        Money $discount
    ) {
        $this->message = $message;
        $this->discount = $discount;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return Money
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
