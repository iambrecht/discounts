<?php

namespace AppBundle\Entity;

use Money\Money;

class OrderItem
{
    private
        $product,
        $quantity,
        $unitPrice,
        $totalPrice;

    public function __construct(
        Product $product,
        $quantity,
        Money $unitPrice,
        Money $totalPrice
    ) {
        $this->product = $product;
        $this->quantity = $quantity;
        $this->unitPrice = $unitPrice;
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return Money
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @return Money
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }
}
