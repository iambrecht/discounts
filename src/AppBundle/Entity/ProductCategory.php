<?php

namespace AppBundle\Entity;

class ProductCategory
{
    const CATEGORY_TOOLS = 1;
    const CATEGORY_SWITCHES = 2;
}
