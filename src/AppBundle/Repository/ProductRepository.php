<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Product;
use AppBundle\Exception\ProductNotFoundException;
use AppBundle\Mapper\ProductMapper;

class ProductRepository
{
    private $productMapper;

    public function __construct(
        ProductMapper $productMapper
    ) {
        $this->productMapper = $productMapper;
    }

    /**
     * @param $productId
     *
     * @return Product
     * @throws ProductNotFoundException
     */
    public function findById($productId)
    {
        foreach ($this->getProducts() as $product) {
            if ($product['id'] === $productId) {
                return $this->productMapper->transformObject($product);
            }
        }

        throw new ProductNotFoundException();
    }

    private function getProducts()
    {
        $json = <<<JSON
[
  {
    "id": "A101",
    "description": "Screwdriver",
    "category": "1",
    "price": "9.75"
  },
  {
    "id": "A102",
    "description": "Electric screwdriver",
    "category": "1",
    "price": "49.50"
  },
  {
    "id": "B101",
    "description": "Basic on-off switch",
    "category": "2",
    "price": "4.99"
  },
  {
    "id": "B102",
    "description": "Press button",
    "category": "2",
    "price": "4.99"
  },
  {
    "id": "B103",
    "description": "Switch with motion detector",
    "category": "2",
    "price": "12.95"
  }
]
JSON;

        return json_decode($json, true);
    }
}
