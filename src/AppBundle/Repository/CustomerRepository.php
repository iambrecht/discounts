<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Customer;
use AppBundle\Exception\CustomerNotFoundException;
use AppBundle\Mapper\CustomerMapper;

class CustomerRepository
{
    private $customerMapper;

    public function __construct (
        CustomerMapper $customerMapper
    ) {
        $this->customerMapper = $customerMapper;
    }

    /**
     * @param $customerId
     *
     * @return Customer
     * @throws CustomerNotFoundException
     */
    public function findById($customerId)
    {
        foreach ($this->getCustomers() as $customer) {
            if ($customer['id'] === $customerId) {
                return $this->customerMapper->transformObject($customer);
            }
        }

        throw new CustomerNotFoundException();
    }

    private function getCustomers()
    {
        $json = <<<JSON
[
  {
    "id": "1",
    "name": "Coca Cola",
    "since": "2014-06-28",
    "revenue": "492.12"
  },
  {
    "id": "2",
    "name": "Teamleader",
    "since": "2015-01-15",
    "revenue": "1505.95"
  },
  {
    "id": "3",
    "name": "Jeroen De Wit",
    "since": "2016-02-11",
    "revenue": "0.00"
  }
]
JSON;

        return json_decode($json, true);
    }
}
