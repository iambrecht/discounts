<?php

namespace AppBundle\Factory;

use AppBundle\Repository\CustomerRepository;
use AppBundle\RuleEngine\DiscountRuleEngine;
use AppBundle\RuleEngine\Rules\Discount\LoyalCustomerRule;
use AppBundle\RuleEngine\Rules\Discount\SixthProductOfEachSwitchCategoryRule;
use AppBundle\RuleEngine\Rules\Discount\TwoProductsOfToolsCategoryRule;

class RuleEngineFactory
{
    public function getRuleEngine($ruleEngineName)
    {
        switch ($ruleEngineName) {
            case 'discount':
                return $this->buildDiscountEngine(); break;
            default:
                throw new \InvalidArgumentException('Unsupported rule engine requested');
        }
    }

    private function buildDiscountEngine()
    {
        return (new DiscountRuleEngine())
            ->addRule(new SixthProductOfEachSwitchCategoryRule())
            ->addRule(new TwoProductsOfToolsCategoryRule())
            ->addRule(new LoyalCustomerRule());
    }
}
