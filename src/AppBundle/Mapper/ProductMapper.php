<?php

namespace AppBundle\Mapper;

use AppBundle\Entity\Product;
use Money\Currency;
use Money\Money;

class ProductMapper
{
    /**
     * @param $objectToTransform
     *
     * @return Product
     */
    public function transformObject($objectToTransform)
    {
        return (new Product())
            ->setId($objectToTransform['id'])
            ->setCategoryId((int) $objectToTransform['category'])
            ->setDescription($objectToTransform['description'])
            ->setPrice(new Money(floatval($objectToTransform['price']) * 100, new Currency('EUR')));
    }
}
