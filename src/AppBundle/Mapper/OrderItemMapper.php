<?php

namespace AppBundle\Mapper;

use AppBundle\Entity\OrderItem;
use AppBundle\Iterator\OrderItemCollection;
use AppBundle\Repository\ProductRepository;
use Money\Currency;
use Money\Money;

class OrderItemMapper
{
    private $productRepository;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
    }

    public function transformObject($objectToTransform)
    {
        return new OrderItem(
            $this->productRepository->findById($objectToTransform['product-id']),
            (int) $objectToTransform['quantity'],
            new Money(floatval($objectToTransform['unit-price']) * 100, new Currency('EUR')),
            new Money(floatval($objectToTransform['total']) * 100, new Currency('EUR'))
        );
    }

    public function transformCollection($collectionToTransform)
    {
        $collection = new OrderItemCollection();

        foreach ($collectionToTransform as $objectToTransform) {
            $collection->addOrderItem($this->transformObject($objectToTransform));
        }

        return $collection;
    }
}
