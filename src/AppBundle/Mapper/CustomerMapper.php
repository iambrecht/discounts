<?php

namespace AppBundle\Mapper;

use AppBundle\Entity\Customer;
use Money\Currency;
use Money\Money;

class CustomerMapper
{
    /**
     * @param $objectToTransform
     *
     * @return Customer
     */
    public function transformObject($objectToTransform)
    {
        return (new Customer())
            ->setId((int) $objectToTransform['id'])
            ->setName($objectToTransform['name'])
            ->setSince(new \DateTime($objectToTransform['since']))
            ->setRevenue(new Money(floatval($objectToTransform['revenue']) * 100, new Currency('EUR')));
    }
}
