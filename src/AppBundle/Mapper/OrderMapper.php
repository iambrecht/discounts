<?php

namespace AppBundle\Mapper;

use AppBundle\Entity\Order;
use AppBundle\Repository\CustomerRepository;
use Money\Currency;
use Money\Money;

class OrderMapper
{
    private
        $customerRepository,
        $orderItemMapper;

    public function __construct(
        CustomerRepository $customerRepository,
        OrderItemMapper $orderItemMapper
    ) {
        $this->customerRepository = $customerRepository;
        $this->orderItemMapper = $orderItemMapper;
    }

    /**
     * @param $objectToTransform
     *
     * @return Order
     */
    public function transformObject($objectToTransform)
    {
        return (new Order())
            ->setId($objectToTransform['id'])
            ->setCustomer($this->customerRepository->findById($objectToTransform['customer-id']))
            ->setOrderItems(
                $this->orderItemMapper->transformCollection($objectToTransform['items'])
            )->setTotal(new Money(floatval($objectToTransform['total']) * 100, new Currency('EUR')));
    }
}
