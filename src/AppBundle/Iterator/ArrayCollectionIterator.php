<?php

namespace AppBundle\Iterator;

class ArrayCollectionIterator implements \IteratorAggregate, \JsonSerializable
{
    protected $collection;

    public function __construct($collection = array())
    {
        $this->collection = $collection;
    }

    protected function add($element)
    {
        $this->collection[] = $element;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->collection);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->collection;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->toArray());
    }

    public function merge(ArrayCollectionIterator $collection)
    {
        $this->collection = array_merge($this->collection, $collection->toArray());

        return $this;
    }

    public function jsonSerialize()
    {
        return array_map(
            function(\JsonSerializable $item){
                return $item->jsonSerialize();
            },
            $this->collection
        );
    }
}
