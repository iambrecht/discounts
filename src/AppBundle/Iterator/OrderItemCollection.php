<?php

namespace AppBundle\Iterator;

use AppBundle\Entity\OrderItem;

class OrderItemCollection extends ArrayCollectionIterator
{
    public function addOrderItem(OrderItem $orderItem)
    {
        $this->add($orderItem);

        return $this;
    }

    public function withCategory($categoryId)
    {
        $filteredOrderItems = new OrderItemCollection();

        foreach ($this->collection as $item) {
            if ($item->getProduct()->getCategoryId() === $categoryId) {
                $filteredOrderItems->addOrderItem($item);
            }
        }

        return $filteredOrderItems;
    }
}
