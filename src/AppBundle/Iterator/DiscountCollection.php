<?php

namespace AppBundle\Iterator;

use AppBundle\Entity\Discount;

class DiscountCollection extends ArrayCollectionIterator
{
    public function addDiscount(Discount $discount)
    {
        $this->add($discount);

        return $this;
    }
}
