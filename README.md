Discounts
=========

#### Available routes
```
POST /discounts/calculate
```

#### Running the application
```
bin/console server:start
```

#### Running unit tests
```
vendor/bin/phpunit --configuration ./phpunit.xml.dist ./tests-unit
```

#### Running functional tests
```
vendor/bin/phpunit --configuration ./phpunit.xml.dist ./tests-functional
```
