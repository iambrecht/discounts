<?php

namespace Tests\TestCase;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiFunctionalTestCase extends WebTestCase
{
    /** @var Client $client */
    protected $client;
    protected $customerNumber;

    public function setUp()
    {
        $this->client = static::createClient();
    }
}
