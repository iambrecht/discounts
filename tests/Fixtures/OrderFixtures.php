<?php

namespace Tests\Fixtures;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Product;
use AppBundle\Iterator\OrderItemCollection;
use Money\Currency;
use Money\Money;

class OrderFixtures
{
    /**
     * @return OrderItem
     */
    public static function getOrderItem(Product $product, $quantity, Money $unitPrice)
    {
        return new OrderItem(
            $product,
            $quantity,
            $unitPrice,
            $unitPrice->multiply($quantity)
        );
    }

    /**
     * @return Order
     */
    public static function getOrder($orderId, Customer $customer, OrderItemCollection $orderItems)
    {
        $totalPrice = new Money(0, new Currency('EUR'));
        foreach ($orderItems as $orderItem) {
            $totalPrice = $totalPrice->add($orderItem->getTotalPrice());
        }

        return (new Order())
            ->setId($orderId)
            ->setCustomer($customer)
            ->setOrderItems($orderItems)
            ->setTotal($totalPrice);
    }
}
