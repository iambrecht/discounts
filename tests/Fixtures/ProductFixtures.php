<?php

namespace Tests\Fixtures;

use AppBundle\Entity\Product;
use Money\Money;

class ProductFixtures
{
    /**
     * @return Product
     */
    public static function getProduct($id, $categoryId, Money $price)
    {
        return (new Product())
            ->setId($id)
            ->setCategoryId($categoryId)
            ->setPrice($price);
    }
}
