<?php

namespace Tests\Fixtures;

use AppBundle\Entity\Customer;

class CustomerFixtures
{
    /**
     * @return Customer
     */
    public static function getCustomer($id, $name, $revenue, $since = null)
    {
        return (new Customer())
            ->setId($id)
            ->setName($name)
            ->setRevenue($revenue)
            ->setSince(is_null($since) ? new \DateTime('now') : $since);
    }
}
